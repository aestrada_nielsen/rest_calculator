/**
 */
package com.restexample.restcalculator.service;

/**
 * 
 * @author alfredo.estrada.consultant@nielsen.com
 */
public interface CalculatorService {
	String getVersion();
	
	void registerArithmeticOperation(ArithmeticOperation arithmeticOperation);
}
