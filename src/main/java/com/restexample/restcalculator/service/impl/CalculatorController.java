/**
 * 
 */
package com.restexample.restcalculator.service.impl;

import com.restexample.restcalculator.service.ArithmeticOperation;
import com.restexample.restcalculator.service.CalculatorService;
import java.util.Hashtable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author alfredo.estrada.consultant@nielsen.com
 */
@RequestMapping("/calculator")
@RestController
public class CalculatorController implements CalculatorService{
	
	private static Hashtable<String,ArithmeticOperation> arithmeticOperationsTable;

	public static Hashtable<String, ArithmeticOperation> getArithmeticOperationsTable() {
		if(arithmeticOperationsTable == null){
			arithmeticOperationsTable = new Hashtable<>();
		}		
		return arithmeticOperationsTable;
	}

	
	@Override
	public void registerArithmeticOperation(ArithmeticOperation arithmeticOperation) {
		getArithmeticOperationsTable().put(arithmeticOperation.getOperationName(), arithmeticOperation);
		System.out.println("------->>register: "+arithmeticOperation.getOperationName()+" : "+arithmeticOperation.getOperationHelp());
	}
	
	
	@GetMapping( value = "/version")
    public String getVersion() {
        return "1.0.0";
    }
	
	@GetMapping( value = "/operations")
	public String getOperations() {		
		return "operations: "+getArithmeticOperationsTable().keySet().toString();
	}


	@GetMapping( value = "/{operationName}/{operandA}/{operandB}")
    public String executeBinaryOperation(@PathVariable String operationName,@PathVariable String operandA,@PathVariable String operandB) {
		ArithmeticOperation op = getArithmeticOperationsTable().get(operationName);
		if(op != null){
			String response = op.performBinaryOperation(operandA, operandB);

			//return "operationName("+operandA+","+operandB+")="+response;
			return response;
		} else {
			return "operationNAme not found:"+operationName;
		}
    }


	
}
