/**
 */
package com.restexample.restcalculator.service.impl;

import com.restexample.restcalculator.service.ArithmeticOperation;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author alfredo.estrada.consultant@nielsen.com
 */
@Service
public class SumOperation implements ArithmeticOperation{
    @Autowired
    private CalculatorController calculatorController;

	@PostConstruct
	public void register() {		
		calculatorController.registerArithmeticOperation(this);
	}

	@Override
	public String getOperationName() {
		return "sum";
	}

	@Override
	public String getOperationHelp() {
		return "execute the arithmetic sum for 2 operands";
	}

	@Override
	public String performBinaryOperation(String operandA, String operandB) {
		
		Double operA  = new Double(operandA);
		Double operB  = new Double(operandB);
		
		Double operR  = operA + operB;
		
		return operR.toString();
	}
	
}
