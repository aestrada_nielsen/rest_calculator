/**
 */
package com.restexample.restcalculator.service;

/**
 *
 * @author alfredo.estrada.consultant@nielsen.com
 */
public interface ArithmeticOperation {
	
	public String getOperationName();
	
	public String getOperationHelp();
	
	public String performBinaryOperation(String operandA, String operandB);
	
}
