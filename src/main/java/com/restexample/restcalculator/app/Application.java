/**
 * 
 */
package com.restexample.restcalculator.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author alfredo.estrada.consultant@nielsen.com
 */
@ComponentScan(basePackages = {"com.restexample.restcalculator.service.impl","com.restexample.restcalculator.app.contextaware"})
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
		System.out.println("--------------------STARTED---------------------");
    }
}
